/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if6ae.exemplo.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Login Servlet", urlPatterns = {"/login"})
public class Login extends HttpServlet {

    protected final int DEFAULT_NUMBER = 5;

    /**
     * Called when the form is filled in by the user.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();

        // The usual HTML setup stuff.
        out.println("<HTML>");
        out.println("<HEAD>");
        out.println("<BODY BGCOLOR=\"white\">");

        // HTML for this page
        out.println("<TITLE>Formulário de Login</TITLE>");

        // Figure out how many numbers to print.
        int n = DEFAULT_NUMBER;

        String login = req.getParameter("login");
        String password = req.getParameter("senha");
        String perfil = req.getParameter("perfil");
                        
        if (!login.equals("") && !password.equals("")) {
            if (login.equals(password)) {
                resp.sendRedirect("/primeiro-servlet/sucesso?login="+login+"&perfil="+perfil);
            } else {
                resp.sendRedirect("/primeiro-servlet/erro.xhtml");
            }
        } else {
            resp.sendRedirect("/primeiro-servlet/erro.xhtml");
        }

    }

}
